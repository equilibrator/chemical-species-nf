#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

/***************************************************************************************
 *  IMPORT LOCAL MODULES/SUBWORKFLOWS
 **************************************************************************************/

include { INITIALIZE_REPOSITORY } from '../modules/local/initialize_repository'
include { CHEMICAL_SPECIES_SQLITE } from '../modules/local/chemical_species_sqlite'

/***************************************************************************************
 *  RUN MAIN WORKFLOW
 **************************************************************************************/

workflow CHEMICAL_SPECIES {
    take:
    structures
    database

    main:

    // If the given SQLite database doesn't exist yet, we initialize a new one.
    ch_db = database.branch {
        old: file(it).exists()
            return file(it)
        new_: true
    }
    INITIALIZE_REPOSITORY(ch_db.new_)

    ch_species_sqlite = structures
        .combine(
            ch_db.old.mix(INITIALIZE_REPOSITORY.out.repos)
        )  // tuple val(meta), path(structures), path(db)
        .multiMap {
            structures: it.take(2)
            db: it.last()
        }

    CHEMICAL_SPECIES_SQLITE(
        ch_species_sqlite.structures,
        ch_species_sqlite.db
    )

}
