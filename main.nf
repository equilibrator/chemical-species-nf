#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

include { FETCH_MNX_TABLE } from './modules/local/fetch_mnx_table'
include { EXTRACT_CHEMICAL_STRUCTURES } from './modules/local/extract_chemical_structures'
include { CHEMICAL_SPECIES } from './workflows/chemical_species'

workflow {
    def options = Channel.of([mnx_version: params.mnx_version])

    if (params.input) {
        structures = options.combine(
            Channel.fromPath(params.input, checkIfExists: true)
        )
    } else {
        FETCH_MNX_TABLE(
            options.map { [it, 'chem_prop.tsv'] }
        )
        EXTRACT_CHEMICAL_STRUCTURES(FETCH_MNX_TABLE.out.structures)
        structures = EXTRACT_CHEMICAL_STRUCTURES.out.structures
    }

    def database = Channel.of(params.database)

    CHEMICAL_SPECIES(structures, database)
}
