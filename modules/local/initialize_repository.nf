process INITIALIZE_REPOSITORY {
    tag "${database}"
    label 'process_low'

    container 'registry.gitlab.com/equilibrator/docker-marvin/marvin:python-3.8_marvin-21.10_7cd05b8_2022-04-11'

    input:
    val database

    output:
    path database, emit: repos

    script:
    def connection_url = "sqlite:///${database}"
    """
    #!/usr/bin/env python


    from equilibrator_cheminfo.infrastructure.persistence.orm import (
        ORMManagementService
    )


    if __name__ == "__main__":
        ORMManagementService.initialize("${connection_url}")

    """
}
