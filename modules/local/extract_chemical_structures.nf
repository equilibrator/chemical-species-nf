process EXTRACT_CHEMICAL_STRUCTURES {
    tag "${chem_prop}"
    label 'process_low'

    conda (params.enable_conda ? 'conda-forge::pandas=1.4.1 conda-forge::pyarrow=7.0.0 conda-forge::zstd=1.5.2 conda-forge::lz4=3.1.10' : null)
    container 'registry.gitlab.com/equilibrator/docker-marvin/marvin:python-3.8_marvin-21.10_7cd05b8_2022-04-11'

    input:
    tuple val(meta), path(chem_prop)

    output:
    tuple val(meta), path('*.arrow'), emit: structures
    path '*.md5',                     emit: md5

    script:
    def mnx_version = meta.mnx_version ?: 'latest'
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: 'structures'
    """
    extract_chemical_structures.py \\
        --mnx-version ${mnx_version} \\
        ${args} \\
        '${chem_prop}' \\
        '${prefix}.arrow'

    md5sum --binary '${prefix}.arrow' > '${prefix}.arrow.md5'
    """
}
