process FETCH_MNX_TABLE {
    tag "${filename}"
    label 'process_low'
    label 'error_retry'

    conda (params.enable_conda ? 'conda-forge::curl=7.68.0' : null)
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://containers.biocontainers.pro/s3/SingImgsRepo/biocontainers/v1.2.0_cv2/biocontainers_v1.2.0_cv2.img' :
        'biocontainers/biocontainers:v1.2.0_cv2' }"

    input:
    tuple val(meta), val(filename)

    output:
    tuple val(meta), path('*.tsv'), emit: structures
    path '*.md5',                   emit: md5
    path 'versions.yml',            emit: versions

    script:
    def mnx_version = meta.mnx_version ?: 'latest'
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${filename}"
    def mnx_server = 'https://www.metanetx.org/ftp'
    """
    curl \\
        --fail \\
        --retry 3 \\
        --location '${mnx_server}/${mnx_version}/${filename}.md5' \\
        --output '${filename}.md5'

    curl \\
        ${args} \\
        --header 'Accept-Encoding: gzip' \\
        --location '${mnx_server}/${mnx_version}/${filename}' \\
        --output '${filename}.gz'

    gzip --decompress --keep '${filename}.gz'

    md5sum --check '${filename}.md5'

    ${prefix != filename ? "mv ${filename} ${prefix}.tsv" : ''}

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        curl: \$(echo \$(curl --version | head -n 1 | sed 's/^curl //; s/ .*\$//'))
    END_VERSIONS
    """
}
