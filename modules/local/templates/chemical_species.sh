#!/usr/bin/env bash

set -u
# We need to allow errors so that we can restart the command.
set +e

retry_ad_infinitum() {
    local attempt=1
    local status=1

    while [ ${status} -ne 0 ]; do
        ${@}
        status=${?}
        if [ ${status} -ne 0 ]; then
            echo "Attempt ${attempt} failed. Restarting..." >&2
            attempt=$(( ${attempt} + 1 ))
        fi
    done
}

retry_ad_infinitum chemical_species.py \
    --processes !{task.cpus} \
    --db-url '!{connection_url}' \
    !{args} \
    '!{structures}'

pigz --best --force --no-time \
    --processes !{task.cpus} \
    '!{database}'

md5sum --binary '!{database}.gz' > '!{database}.gz.md5'
