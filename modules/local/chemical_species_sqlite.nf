process CHEMICAL_SPECIES_SQLITE {
    tag "${structures}"
    label 'maximum_process'

    container 'registry.gitlab.com/equilibrator/docker-marvin/marvin:python-3.8_marvin-21.10_7cd05b8_2022-04-11'
    containerOptions '-i --publish 127.0.0.1:8265:8265 --shm-size=30gb'

    // We copy any existing SQLite file in order to be able to resume from the same point.
    stageInMode 'copy'

    input:
    tuple val(meta), path(structures)
    path database

    output:
    tuple val(meta), path("${database}.gz"), emit: repos
    path '*.md5',                            emit: md5

    shell:
    args = task.ext.args ?: "${ workflow.containerEngine ? '--host 0.0.0.0 --force-interactive' : '' }"
    connection_url = "sqlite:///${database}"
    template 'chemical_species.sh'
}
