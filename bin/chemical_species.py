#!/usr/bin/env python


# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Provide a service for estimating proton dissociation constants."""


import argparse
import logging
import os
import re
import sys
from pathlib import Path
from typing import Iterable, List, Optional

import pandas as pd
import ray
import rich.progress as rprog
from equilibrator_cheminfo.domain.model import (
    AbstractChemicalSpeciesRepository,
    ChemicalSpecies,
    Structure,
    StructuresTable,
)
from equilibrator_cheminfo.infrastructure import CheminformaticsBackend, DomainRegistry
from equilibrator_cheminfo.infrastructure.chemaxon import (
    ChemAxonChemicalSpeciesConfiguration,
)
from equilibrator_cheminfo.infrastructure.persistence.orm import ORMManagementService
from ray.util import ActorPool
from rich.console import Console
from rich.logging import RichHandler
from tenacity import Retrying, stop_after_attempt


logger = logging.getLogger(__name__)


class TaskException(Exception):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args)
        self.__dict__.update(kwargs)


@ray.remote(max_restarts=-1, max_task_retries=-1)
class ChemAxonAgent:
    """Define a ChemAxon-based ray agent for creating chemical species."""

    def __init__(
        self,
        configuration: ChemAxonChemicalSpeciesConfiguration,
        log_level: int,
        **kwargs,
    ) -> None:
        """Initialize the ChemAxon chemical species factory."""
        super().__init__(**kwargs)
        logging.basicConfig(level=log_level, format="[%(levelname)s] %(message)s")
        self._logger = logging.getLogger(self.__class__.__name__)
        self._factory = DomainRegistry.chemical_species_factory(configuration)

    def execute(self, structure: Structure) -> Optional[ChemicalSpecies]:
        """Attempt to create a chemical species."""
        self._logger.debug(structure.identifier.inchikey)
        try:
            for attempt in Retrying(stop=stop_after_attempt(3), reraise=True):
                with attempt:
                    return self._factory.make(structure=structure)
        except Exception as error:
            raise TaskException(str(error), structure=structure) from None


def parse_arguments(argv: Optional[List[str]] = None) -> argparse.Namespace:
    """Define the command line arguments and immediately parse them."""
    num_processes = len(os.sched_getaffinity(0))
    if not num_processes:
        num_processes = 1
    if num_processes > 1:
        num_processes -= 1

    parser = argparse.ArgumentParser(
        prog="marvin",
        description="Build a molecular entity database containing proton dissociation "
        "constants and major microspecies using ChemAxon Marvin.",
    )
    parser.add_argument(
        "--db-url",
        required=True,
        metavar="URL",
        help="A string interpreted as an rfc1738 compatible database URL.",
    )
    default_log_level = "INFO"
    parser.add_argument(
        "-l",
        "--log-level",
        default=default_log_level,
        help=f"The desired log level (default {default_log_level}).",
        choices=("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"),
    )
    parser.add_argument(
        "--minimum-ph",
        type=float,
        default=0.0,
        help="The minimum pH value to consider (default 0).",
    )
    parser.add_argument(
        "--maximum-ph",
        type=float,
        default=14.0,
        help="The maximum pH value to consider (default 14).",
    )
    parser.add_argument(
        "--ph",
        type=float,
        default=7.0,
        help="The pH value at which to determine the major microspecies (default 7).",
    )
    parser.add_argument(
        "--large-model",
        action="store_true",
        help="Use a model for pKa values estimation that is suitable for large "
        "molecules. Without this flag a small model is used which is faster and works "
        "fine for most metabolites.",
    )
    parser.add_argument(
        "--backend",
        choices=("ChemAxon",),
        default="ChemAxon",
        help="The cheminformatics backend (default ChemAxon).",
    )
    parser.add_argument(
        "--processes",
        type=int,
        default=num_processes,
        help=f"The number of parallel processes to start (default {num_processes}).",
    )
    default_host = "127.0.0.1"
    parser.add_argument(
        "--host",
        default=default_host,
        help=f"The host address (default {default_host}).",
    )
    parser.add_argument(
        "--force-interactive",
        action="store_true",
        help="Force an interactive terminal for progress display. This can be helpful "
        "when program output is piped to a file or run in a container.",
    )
    parser.add_argument(
        "structures",
        metavar="STRUCTURES",
        type=Path,
        help="Path to a TSV file with chemical structures defined by the columns "
        "inchikey, inchi, smiles. Missing values are allowed.",
    )

    return parser.parse_args(argv)


def make_configuration(
    backend: CheminformaticsBackend, args: argparse.Namespace
) -> ChemAxonChemicalSpeciesConfiguration:
    if backend is CheminformaticsBackend.ChemAxon:
        from equilibrator_cheminfo.infrastructure.chemaxon import (
            ChemAxonChemicalSpeciesConfiguration,
        )

        return ChemAxonChemicalSpeciesConfiguration(
            cheminformatics_backend=backend,
            minimum_ph=args.minimum_ph,
            maximum_ph=args.maximum_ph,
            fixed_ph=args.ph,
            use_large_model=args.large_model,
        )
    else:
        raise ValueError(
            "Currently, pKa and major microspecies estimation is only provided by "
            "ChemAxon Marvin."
        )


def make_repository(url: str) -> AbstractChemicalSpeciesRepository:
    sqlite = re.compile(r"sqlite.*?:///")
    if (match := sqlite.match(url)) is not None:
        if not (filename := Path(url[match.end() :])).is_file():
            logger.info("Set up clean database at %s.", str(filename))
            filename.parent.mkdir(parents=True, exist_ok=True)
            ORMManagementService.initialize(url)
    return DomainRegistry.chemical_species_repository(url)


def filter_keys(
    structures: pd.DataFrame, repository: AbstractChemicalSpeciesRepository
) -> pd.DataFrame:
    """Filter the structures by known InChIKeys."""
    keys = repository.get_inchikeys()
    logger.info(f"{len(keys):n} InChIKeys in the repository.")
    mask = ~structures["inchikey"].isin(keys)
    logger.info(f"{mask.sum():n} chemical structures not yet in the repository.")
    return structures.loc[mask, :].copy()


def progress_bar(console: Console) -> rprog.Progress:
    """Create a rich progress bar."""
    return rprog.Progress(
        rprog.TextColumn("[bold blue]{task.description}", justify="right"),
        rprog.BarColumn(bar_width=None),
        "[progress.percentage]{task.completed:,}/{task.total:,}({task.percentage:>3.1f}%)",
        " ",
        rprog.TimeRemainingColumn(),
        " ",
        rprog.TimeElapsedColumn(),
        console=console,
    )


def extract_structures(filename: Path) -> pd.DataFrame:
    """"""
    extensions = frozenset(filename.suffixes)
    if extensions.intersection({".arrow"}):
        return pd.read_feather(filename)
    elif extensions.intersection({".tsv"}):
        return pd.read_table(filename, sep="\t")
    elif extensions.intersection({".csv"}):
        return pd.read_csv(filename)
    else:
        raise ValueError(f"Currently unsupported file format {filename}.")


def iter_results(pool: ActorPool, total: int) -> Iterable[Optional[ChemicalSpecies]]:
    """"""
    done_count = 0
    while done_count < total:
        result = None
        try:
            result = pool.get_next_unordered()
        except TaskException as error:
            logger.debug("", exc_info=error)
            logger.error(
                "Predicting chemical species for structure %s failed due to error: %s",
                error.structure.identifier.inchikey,
                str(error),
            )
        finally:
            yield result
            done_count += 1


def run(
    structures: pd.DataFrame,
    repository: AbstractChemicalSpeciesRepository,
    configuration: ChemAxonChemicalSpeciesConfiguration,
    processes: int,
    console: Console,
) -> None:
    pool = ActorPool(
        [
            ChemAxonAgent.remote(
                configuration=configuration, log_level=logger.getEffectiveLevel()
            )
            for _ in range(processes)
        ]
    )
    with progress_bar(console) as pbar:
        task = pbar.add_task(description="Submit Task", total=len(structures))
        for structure in structures.cheminfo.iter_structures():
            pool.submit(lambda actor, value: actor.execute.remote(value), structure)
            pbar.update(task, advance=1)
        task = pbar.add_task(description="Structure ➔ Species", total=len(structures))
        for species in iter_results(pool, len(structures)):
            if species is not None:
                repository.add(species)
            pbar.update(task, advance=1)


def main(argv: Optional[List[str]] = None) -> None:
    """Coordinate argument parsing and command calling."""
    args = parse_arguments(argv)
    if args.force_interactive:
        console = Console(force_terminal=True, force_interactive=True, file=sys.stderr)
    else:
        console = Console(file=sys.stderr)
    logging.basicConfig(
        level=args.log_level,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler(markup=True, rich_tracebacks=True)],
    )
    assert (
        args.structures.is_file()
    ), f"Chemical structures not found at {args.structures}."
    config = make_configuration(CheminformaticsBackend(args.backend), args)
    repo = make_repository(args.db_url)
    structures = filter_keys(
        StructuresTable.make_from_table(extract_structures(args.structures)), repo
    )
    ray.init(num_cpus=args.processes, dashboard_port=8265, dashboard_host="0.0.0.0")
    run(structures, repo, config, args.processes, console)


if __name__ == "__main__":
    main()
