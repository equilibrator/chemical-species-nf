#!/usr/bin/env python


# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Provide a command line tool for extracting chemical structures."""


import argparse
import logging
from pathlib import Path
from typing import List, Optional

import pandas as pd

logger = logging.getLogger(__name__)


TABLE_CONFIG = {
    "4.4": (
        (
            "mnx_id",
            "name",
            "source",
            "formula",
            "charge",
            "mass",
            "inchi",
            "inchikey",
            "smiles",
        ),
        352,
    )
}


def extract_chem_prop(filename: Path, version: str) -> pd.DataFrame:
    """Extract a chem_prop table using a specific version's configuration."""
    columns, skip = TABLE_CONFIG[version]
    return pd.read_table(filename, sep="\t", header=None, names=columns, skiprows=skip)


def transform_chem_prop(table: pd.DataFrame) -> pd.DataFrame:
    """Transform InChIKey column and return structure information only."""
    table["inchikey"] = table["inchikey"].str[len("InChIKey=") :]
    return table[["inchikey", "inchi", "smiles"]]


def parse_arguments(argv: Optional[List[str]] = None) -> argparse.Namespace:
    """Define the command line arguments and immediately parse them."""
    parser = argparse.ArgumentParser(
        description="Extract chemical structures from a MetaNetX chem_prop TSV file.",
    )
    default_log_level = "INFO"
    parser.add_argument(
        "-l",
        "--log-level",
        default=default_log_level,
        help=f"The desired log level (default {default_log_level}).",
        choices=("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"),
    )
    default_mnx_version = "latest"
    parser.add_argument(
        "--mnx-version",
        default=default_mnx_version,
        help=f"The MetaNetX version (default {default_mnx_version}).",
        choices=("4.4", "latest"),
    )
    default_compression = "zstd"
    parser.add_argument(
        "--compression",
        default=default_compression,
        help=f"The compression method to use (default {default_compression}).",
        choices=("zstd", "lz4"),
    )
    default_compression_level = 19
    parser.add_argument(
        "--compression-level",
        default=default_compression_level,
        type=int,
        help=f"The compression-level to use (default {default_compression_level}). "
        f"Must be between zero and nineteen inclusive.",
    )
    parser.add_argument(
        "chem_prop",
        metavar="CHEM_PROP",
        type=Path,
        help="Input in the form of a MetaNetX chem_prop TSV file.",
    )
    parser.add_argument(
        "structures",
        metavar="STRUCTURES",
        type=Path,
        help="Output path to an Apache Feather file with chemical structures defined "
        "by the columns inchikey, inchi, and smiles. Missing values are allowed.",
    )
    return parser.parse_args(argv)


def main(argv: Optional[List[str]] = None) -> None:
    """Coordinate argument parsing and command calling."""
    args = parse_arguments(argv)
    logging.basicConfig(level=args.log_level, format="[%(levelname)s] %(message)s")
    if args.mnx_version == "latest":
        args.mnx_version = max(TABLE_CONFIG)
    assert (
        args.mnx_version in TABLE_CONFIG
    ), f"Unsupported MetaNetX version {args.mnx_version}."
    assert (
        0 <= args.compression_level <= 19
    ), f"Unsupported compression level {args.compression_level}."
    assert args.chem_prop.is_file(), f"File not found {args.chem_prop}."
    args.structures.parent.mkdir(parents=True, exist_ok=True)
    logger.info("Extract chem_prop table.")
    raw = extract_chem_prop(args.chem_prop, args.mnx_version)
    logger.info("Transform table entries.")
    df = transform_chem_prop(raw)
    logger.info("Write chemical structures to Apache Feather format.")
    df.to_feather(
        args.structures,
        compression=args.compression,
        compression_level=args.compression_level,
    )


if __name__ == "__main__":
    main()
